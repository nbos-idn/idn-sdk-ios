Pod::Spec.new do |s|

# 1
s.platform = :ios
s.ios.deployment_target = '8.0'
s.name = "idn_sdk_ios"
s.summary = "idn_sdk_ios "
s.requires_arc = true

# 2
s.version = "0.3.6"

# 3
s.license = { :type => "MIT", :file => "LICENSE" }

# 4 - Replace with your name and e-mail address
s.author = { "Afsara" => "afsarunnisa@wavelabs.in" }

# For example,
# s.author = { "Joshua Greene" => "jrg.developer@gmail.com" }


# 5 - Replace this URL with your own Github page's URL (from the address bar)
s.homepage = "https://gitlab.com/wavelabs/idn-sdk-ios"

# For example,
# s.homepage = "https://github.com/JRG-Developer/RWPickFlavor"


# 6 - Replace this URL with your own Git URL from "Quick Setup"
s.source = { :git => "https://gitlab.com/wavelabs/idn-sdk-ios.git", :tag => "#{s.version}"}

# For example,
# s.source = { :git => "https://github.com/JRG-Developer/RWPickFlavor.git", :tag => "#{s.version}"}


# 7
s.framework = "UIKit"
s.dependency 'Alamofire'
s.dependency 'MBProgressHUD'

# 8
s.source_files = "idn_sdk_ios/**/*.{swift}"
s.source_files = "idn_sdk_ios/Modules/Identity/*.{swift}"
s.source_files = "idn_sdk_ios/Modules/IDS/*.{swift}"
s.source_files = "idn_sdk_ios/Modules/Media/*.{swift}"
s.source_files = "idn_sdk_ios/API/Models/*.{swift}"
s.source_files = "idn_sdk_ios/API/**/*.{swift}"

# 9
#s.resources = "idn_sdk_ios/**/*.{png,jpeg,jpg,storyboard,xib}"
end
