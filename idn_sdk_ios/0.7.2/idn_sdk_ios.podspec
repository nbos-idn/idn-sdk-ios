Pod::Spec.new do |s|

# 1
s.platform = :ios
s.ios.deployment_target = '9.0'
s.name = "idn_sdk_ios"
s.summary = "idn_sdk_ios"
s.requires_arc = true

# 2
s.version = "0.7.2"

# 3
s.license = { :type => "MIT", :file => "LICENSE" }

# 4 - Replace with your name and e-mail address
s.author = { "Afsara" => "afsarunnisa@wavelabs.in" }


# 5 - Replace this URL with your own Github page's URL (from the address bar)
s.homepage = "https://gitlab.com/wavelabs/idn-sdk-ios"


# 6 - Replace this URL with your own Git URL from "Quick Setup"
s.source = { :git => "https://gitlab.com/wavelabs/idn-sdk-ios.git", :tag => "#{s.version}"}



# 7
s.framework = "UIKit"
s.dependency 'Alamofire', '~> 4.4'
s.dependency 'MBProgressHUD'
s.dependency 'SwiftyJSON'

# 8
s.source_files = "idn_sdk_ios/**/*.{swift}"



# 9
#s.resources = "idn_sdk_ios/**/*.{png,jpeg,jpg,storyboard,xib}"
end
